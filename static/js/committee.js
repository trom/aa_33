document.addEventListener("DOMContentLoaded", function() {

  document.getElementById("show-committee").addEventListener("click", function() {
    document.getElementById("committee").hidden = null;
  });

  var form = document.getElementById("apply");

  form.addEventListener("submit", function(event) {
    event.preventDefault();
    event.stopPropagation();
    var params = "";
    var fields = form.elements;
    for (var i = 0; i < fields.length - 2; i++)
      params += fields[i].name + "=" + encodeURIComponent(fields[i].value) + "&";

    params = params.slice(0, -1);

    var request = new XMLHttpRequest();
    request.open("POST", "/committee/apply/submit", true);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    request.addEventListener("readystatechange", function() {
      if (request.readyState == 4) {
        if (request.status >= 200 && request.status < 300) {
          document.getElementById("success-message").hidden = null;
          document.getElementById("error-message").hidden = true;
        } else {
          document.getElementById("success-message").hidden = true;
          document.getElementById("error-message").hidden = null;
        }
      }
    }, false);

    request.send(params);
    return false;
  });
});
