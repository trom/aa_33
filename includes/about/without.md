## {{ without-heading[heading] Without Acceptable Ads }}

{{ without-1 Overwhelming amounts of poor-quality online advertising exists. This disrupts the user experience and leads to huge amounts of users installing ad blockers. }}

{{ without-2 Users are frustrated, advertisers miss out on an important audience, and publishers miss out on advertising revenue. Everyone loses. }}
