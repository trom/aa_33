## {{standards-placement-heading[Standards placement heading] <span>03</span> Size }}

{{standards-placement-body-text[Standards placement body text] There are strict sizes for individual ads, as well as the total space occupied by ads. All ads that are visible in the browser window when the page first loads must not collectively occupy more than 15% of the visible portion of the web page. If placed lower on the page, ads must not collectively occupy more than 25% of the visible portion of the webpage.}}

{{standards-placement-list-text[Standards placement list text] Ads must also comply with size limitations, according to the ad position:}}

  - 200px high when above the Primary Content
  - 350px wide when on the side of the Primary Content
  - 400px high when placed below the Primary Content
