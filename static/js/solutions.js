function gotoPage(name) {
  var hrefParts = window.location.href.split("/");
  window.location.href = hrefParts[0]
    + "/" + (hrefParts[1] || "en")
    + "/" + name;
}


document.addEventListener("DOMContentLoaded", function() {

    document.getElementById("show-partners").addEventListener("click", function() {
      document.getElementById("partners").hidden = null;
      document.getElementById("show-partners").hidden = true;
    });

    document.getElementById("hide-partners").addEventListener("click", function() {
      document.getElementById("partners").hidden = true;
      document.getElementById("show-partners").hidden = null;
    });

    var form = document.getElementById("apply");
    var errorElement = document.getElementById("error-message");
    var SUBMISSION_ERROR = document.getElementById("submission-error").textContent;

    function showError(message) {
      errorElement.textContent = message;
      form.className = form.className + " error";
    }

    form.addEventListener("submit", function(event) {

      event.preventDefault();

      var data = new FormData(form);
      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            gotoPage("thank-you");
          } else {
            if (console && console.error) {
              console.error(data, arguments);
            }
            showError(SUBMISSION_ERROR);
          }
        }
      };

      xhr.open("POST", "https://hooks.zapier.com/hooks/catch/3616879/qanuve/", true);
      xhr.send(data);
    });

  });
